<?php

namespace App\Repository;

use App\Repository\TaskTypeRepositoryInterface;
use App\Entity\TaskType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class TaskTypeRepository implements TaskTypeRepositoryInterface
{

    private $entityManager;
    private $objectRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(TaskType::class);
    }

    public function findAll(): array
    {
        return $this->objectRepository->findall();
    }

    public function findAllPaged(int $page, int $size, string $search = ''): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('tt')
            ->from(TaskType::class, 'tt')
            ->setFirstResult($page)
            ->setMaxResults($size);

        if ($search !== '') {
            $qb->where('lower(tt.name) LIKE :search');
            $qb->orWhere('lower(tt.type) LIKE :search');
            $qb->setParameter('search', '%'. strtolower($search) .'%');
        }

        $query = $qb->getQuery();
        $sql = $query->getSQL();
        $paginator = new Paginator($query);
        $total = count($paginator);

        return [
            'data' => $query->getResult(),
            'pagination' => [
                'page' => $page + 1,
                'pagesTotal' => (int) ceil($total / $size),
                'recordsTotal' => $total,
                'size' => $size
            ]
        ];
    }

    public function findOneById(int $id): ?TaskType
    {
        return $this->objectRepository->find($id);
    }

    public function findByName(string $name): array
    {
        return $this->objectRepository->findBy(['name'=> $name]);
    }

    public function findByType(string $type): array
    {
        return $this->objectRepository->findBy(['type'=> $type]);
    }

    public function save(TaskType $taskType): void
    {
        $this->entityManager->persist($taskType);
        $this->entityManager->flush();
    }

    public function delete(TaskType $taskType): void
    {
        $this->entityManager->remove($taskType);
        $this->entityManager->flush();
    }
}