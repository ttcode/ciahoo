<?php

namespace App\Repository;

use App\Entity\User;
use App\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class UserRepository implements UserRepositoryInterface
{
    private $entityManager;
    private $objectRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(User::class);
    }

    public function findById(int $id): ?User
    {
        return $this->objectRepository->find($id);
    }

    public function findByUsername(string $username): ?User
    {
        return $this->objectRepository->findOneBy(['username' => $username]);
    }

    public function findAll(): array
    {
        return $this->objectRepository->findall();
    }

    public function findAllPaged(int $page, int $size, string $search = ''): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('u')
            ->from(User::class, 'u')
            ->setFirstResult($page)
            ->setMaxResults($size);

        if ($search !== '') {
            $qb->where('lower(u.username) LIKE :search');
            $qb->orWhere('lower(u.email) LIKE :search');
            $qb->setParameter('search', '%'. strtolower($search) .'%');
        }

        $query = $qb->getQuery();
        $sql = $query->getSQL();
        $paginator = new Paginator($query);
        $total = count($paginator);

        return [
            'data' => $query->getResult(),
            'pagination' => [
                'page' => $page + 1,
                'pagesTotal' => (int) ceil($total / $size),
                'recordsTotal' => $total,
                'size' => $size
            ]
        ];
    }

    public function delete(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}