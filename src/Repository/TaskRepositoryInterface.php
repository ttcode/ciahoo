<?php

namespace App\Repository;

use App\Entity\Task;

interface TaskRepositoryInterface {

    public function findById(int $id): ?Task;

    public function findAll(): array;

    public function findAllPaged(int $page, int $size, string $search): array;

    public function save(Task $task): void;

    public function delete(Task $task): void;

}