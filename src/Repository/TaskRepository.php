<?php

namespace App\Repository;

use App\Entity\Task;
use App\Repository\TaskRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class TaskRepository implements TaskRepositoryInterface
{

    private $entityManager;
    private $objectRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Task::class);
    }

    public function findById(int $id): ?Task
    {
        return $this->objectRepository->find($id);
    }

    public function findAll(): array
    {
        return $this->objectRepository->findall();
    }

    public function findAllPaged(int $page, int $size, string $search = ''): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('t')
            ->from(Task::class, 't')
            ->setFirstResult($page)
            ->setMaxResults($size);

        if ($search !== '') {
            $qb->where('lower(t.description) LIKE :search');
            $qb->setParameter('search', '%'. strtolower($search) .'%');
        }

        $query = $qb->getQuery();
        $sql = $query->getSQL();
        $paginator = new Paginator($query);
        $total = count($paginator);

        return [
            'data' => $query->getResult(),
            'pagination' => [
                'page' => $page + 1,
                'pagesTotal' => (int) ceil($total / $size),
                'recordsTotal' => $total,
                'size' => $size
            ]
        ];
    }

    public function delete(Task $task): void
    {
        $this->entityManager->remove($task);
        $this->entityManager->flush();
    }

    public function save(Task $task): void
    {
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    public function getTasksCountOnPlannedDate($plannedDate)
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('count(t.id) as quantity')
            ->from(Task::class, 't')
            ->where('t.plannedDate = :planned_date')
            ->setParameter('planned_date', $plannedDate);
        $query = $qb->getQuery();
        return $query->getSingleResult();
    }

}