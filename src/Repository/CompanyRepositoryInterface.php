<?php

namespace App\Repository;

use App\Entity\Company;

interface CompanyRepositoryInterface {

    public function findById(int $id): ?Company;

    public function findAll(): array;

    public function findAllPaged(int $page, int $size, string $search): array;

    public function save(Company $task): void;

    public function delete(Company $task): void;

}