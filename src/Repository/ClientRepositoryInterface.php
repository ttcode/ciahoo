<?php

namespace App\Repository;

use App\Entity\Client;

interface ClientRepositoryInterface {

    public function findAll(): array;

    public function findAllPaged(int $page, int $size, string $search): array;

    public function findOneById(int $id): ?Client;

    public function findByState(int $state): array;

    public function findByCity(string $city): array;

    public function save(Client $client): void;

    public function delete(Client $client): void;

}