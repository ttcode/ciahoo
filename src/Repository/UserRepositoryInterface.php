<?php

namespace App\Repository;

use App\Entity\User;

interface UserRepositoryInterface {

    public function findById(int $id): ?User;

    public function findByUsername(string $username): ?User;

    public function findAll(): array;

    public function findAllPaged(int $page, int $size, string $search): array;

    public function save(User $task): void;

    public function delete(User $task): void;

}