<?php

namespace App\Repository;

use App\Entity\Product;

interface ProductRepositoryInterface {

    public function findById(int $id): ?Product;

    public function findAll(): array;

    public function findAllPaged(int $page, int $size, string $search): array;

    public function save(Product $product): void;

    public function delete(Product $product): void;

}