<?php

namespace App\Repository;

use App\Entity\TaskType;

interface TaskTypeRepositoryInterface
{
    public function findAll(): array;

    public function findAllPaged(int $page, int $size, string $search): array;

    public function findOneById(int $id): ?TaskType;

    public function findByType(string $type): array;

    public function findByName(string $type): array;

    public function save(TaskType $taskType): void;

    public function delete(TaskType $taskType): void;
}