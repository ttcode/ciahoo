<?php

namespace App\Repository;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ProductRepository implements ProductRepositoryInterface
{

    private $entityManager;
    private $objectRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Product::class);
    }

    public function findById(int $id): ?Product
    {
        return $this->objectRepository->find($id);
    }

    public function findAll(): array
    {
        return $this->objectRepository->findall();
    }
    
    public function findAllPaged(int $page, int $size, string $search = ''): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('p')
            ->from(Product::class, 'p')
            ->setFirstResult($page)
            ->setMaxResults($size);

        if ($search !== '') {
            $qb->where('lower(p.name) LIKE :search');
            $qb->orWhere('lower(p.symbol) LIKE :search');
            $qb->orWhere('lower(p.description) LIKE :search');
            $qb->setParameter('search', '%'. strtolower($search) .'%');
        }

        $query = $qb->getQuery();
        $sql = $query->getSQL();
        $paginator = new Paginator($query);
        $total = count($paginator);

        return [
            'data' => $query->getResult(),
            'pagination' => [
                'page' => $page + 1,
                'pagesTotal' => (int) ceil($total / $size),
                'recordsTotal' => $total,
                'size' => $size
            ]
        ];
    }

    public function delete(Product $product): void
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }

    public function save(Product $product): void
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

}