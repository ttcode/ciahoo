<?php

namespace App\Repository;

use App\Entity\Client;
use App\Repository\ClientRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ClientRepository implements ClientRepositoryInterface {

    private $entityManager;
    private $objectRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Client::class);
    }

    public function findAll(): array 
    {
        return $this->objectRepository->findall();
    }

    public function findAllPaged(int $page, int $size, string $search = ''): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('c')
            ->from(Client::class, 'c')
            ->setFirstResult($page)
            ->setMaxResults($size);

        if ($search !== '') {
            $qb->where('lower(c.firstname) LIKE :search');
            $qb->orWhere('lower(c.lastname) LIKE :search');
            $qb->orWhere('lower(c.city) LIKE :search');
            $qb->orWhere('lower(c.street) LIKE :search');
            $qb->orWhere('lower(c.postcode) LIKE :search');
            $qb->orWhere('lower(c.email) LIKE :search');
            $qb->orWhere('lower(c.phone) LIKE :search');
            $qb->orWhere('lower(c.country) LIKE :search');
            $qb->setParameter('search', '%'. strtolower($search) .'%');
        }

        $query = $qb->getQuery();
        $sql = $query->getSQL();
        $paginator = new Paginator($query);
        $total = count($paginator);

        return [
            'data' => $query->getResult(),
            'pagination' => [
                'page' => $page + 1,
                'pagesTotal' => (int) ceil($total / $size),
                'recordsTotal' => $total,
                'size' => $size
            ]
        ];
    }

    public function findOneById(int $id): ?Client
    {
        return $this->objectRepository->find($id);
    }

    public function findByCity(string $city): array
    {
        return $this->objectRepository->findBy(['city'=> $city]);
    }

    public function findByState(int $state): array
    {
        return $this->objectRepository->findBy(['state'=> $state]);
    }

    public function save(Client $client): void
    {
        $this->entityManager->persist($client);
        $this->entityManager->flush();
    }

    public function delete(Client $client): void
    {
        $this->entityManager->remove($client);
        $this->entityManager->flush();
    }

}