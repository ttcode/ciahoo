<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191214191502 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE client ADD firstname VARCHAR(200) NOT NULL');
        $this->addSql('ALTER TABLE client ADD lastname VARCHAR(200) NOT NULL');
        $this->addSql('ALTER TABLE client DROP first_name');
        $this->addSql('ALTER TABLE client DROP last_name');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE client ADD first_name VARCHAR(200) NOT NULL');
        $this->addSql('ALTER TABLE client ADD last_name VARCHAR(200) NOT NULL');
        $this->addSql('ALTER TABLE client DROP firstname');
        $this->addSql('ALTER TABLE client DROP lastname');
    }
}
