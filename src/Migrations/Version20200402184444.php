<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200402184444 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE task_type ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task_type ADD type VARCHAR(250) DEFAULT NULL');
        $this->addSql('ALTER TABLE task_type ADD name VARCHAR(250) DEFAULT NULL');
        $this->addSql('ALTER TABLE task_type ADD description VARCHAR(500) DEFAULT NULL');
        $this->addSql('ALTER TABLE task_type ADD CONSTRAINT FK_FF6DC352979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FF6DC352979B1AD6 ON task_type (company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE task_type DROP CONSTRAINT FK_FF6DC352979B1AD6');
        $this->addSql('DROP INDEX IDX_FF6DC352979B1AD6');
        $this->addSql('ALTER TABLE task_type DROP company_id');
        $this->addSql('ALTER TABLE task_type DROP type');
        $this->addSql('ALTER TABLE task_type DROP name');
        $this->addSql('ALTER TABLE task_type DROP description');
    }
}
