<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191123173120 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE task_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE task (id INT NOT NULL, description VARCHAR(255) DEFAULT NULL, planned_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, client_id INT DEFAULT NULL, user_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE tasks_products (task_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(task_id, product_id))');
        $this->addSql('CREATE INDEX IDX_B73131358DB60186 ON tasks_products (task_id)');
        $this->addSql('CREATE INDEX IDX_B73131354584665A ON tasks_products (product_id)');
        $this->addSql('ALTER TABLE tasks_products ADD CONSTRAINT FK_B73131358DB60186 FOREIGN KEY (task_id) REFERENCES task (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tasks_products ADD CONSTRAINT FK_B73131354584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tasks_products DROP CONSTRAINT FK_B73131358DB60186');
        $this->addSql('DROP SEQUENCE task_id_seq CASCADE');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE tasks_products');
    }
}
