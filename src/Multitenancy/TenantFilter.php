<?php

namespace App\Multitenancy;

use App\Multitenancy\TenantAware;
use App\Multitenancy\Tenant;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class TenantFilter extends SQLFilter
{
    private $company;

    public function setCompany(Tenant $tenant)
    {
        $this->company = $tenant;
    }

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (!$this->company) {
            return "";
        }

        if (!$targetEntity->reflClass->implementsInterface(TenantAware::class)) {
            return "";
        }

        $condition = $targetTableAlias . '.company_id = ' . $this->company->getId();
        return $condition;
    }
}