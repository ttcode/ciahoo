<?php

namespace App\Multitenancy;

final class TokenDecoder
{
    public function decode(string $token): string
    {
        $subToken = substr($token, 6);
        $tokenParts = explode(".", $subToken);
        $tokenPayload = json_decode(base64_decode($tokenParts[1]));
        return $tokenPayload->username;
    }
}