<?php

namespace App\Multitenancy;

interface Tenant {
    public function getId(): ?int;
}