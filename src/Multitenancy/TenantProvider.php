<?php

namespace App\Multitenancy;

use App\Entity\User;
use App\Multitenancy\Tenant;
use App\Repository\CompanyRepository;
use Exception;

class TenantProvider
{
    private $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function findByLoggedUser(User $user): ?Tenant
    {
        $company = $this->companyRepository->findById($user->getCompany()->getId());

        if (!$company->getIsActive()) {
            throw new Exception('Company not active', 500);
        }

        return $company;
    }
}