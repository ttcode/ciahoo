<?php

namespace App\Multitenancy;

use App\Entity\User;
use Exception;

class TenantResolver
{
    private $tenantProvider;

    public function __construct(TenantProvider $tenantProvider)
    {
        $this->tenantProvider = $tenantProvider;
    }

    public function resolve(User $user): Tenant
    {
        $company = $this->tenantProvider->findByLoggedUser($user);

        if (!$company) {
            throw new Exception('Company not found', 404);
        }

        return $company;
    }
}