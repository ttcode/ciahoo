<?php

namespace App\Multitenancy;

use Exception;

class TenantContext
{
    private $company;
    private $isInitialized = false;

    public function initialize(Tenant $company)
    {
        if ($this->isInitialized) {
            throw new Exception('Company context already initialized', 500);
        }

        $this->isInitialized = true;
        $this->company = $company;
    }

    public function getCurrentCompany(): Tenant {
        if (is_null($this->company)) {
            throw new Exception('No company context', 500);
        }

        return $this->company;
    }
}