<?php

namespace App\Multitenancy;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;
use InvalidArgumentException;

class TenantListener
{
    private $tenantContext;

    public function __construct(TenantContext $tenantContext)
    {
        $this->tenantContext = $tenantContext;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof TenantAware) {
            try {
                $tenant = $this->tenantContext->getCurrentCompany();
                $entity->setCompany($tenant);
            } catch (Exception $e) {
                if (is_null($entity->getCompany())) {
                    throw new InvalidArgumentException('Company has to be set');
                }
            }
        }
    }
}