<?php

namespace App\Multitenancy;

use App\Entity\Company;

interface TenantAware {
    public function getCompany(): ?Company;
    public function setCompany(Company $company);
}