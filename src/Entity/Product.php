<?php 

namespace App\Entity;

use App\Multitenancy\TenantAware;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 * @Serializer\ExclusionPolicy("all")
 */
class Product implements TenantAware
{

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Serializer\Expose
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Serializer\Expose
     */
    private $symbol;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="Task", mappedBy="products")
     */
    private $tasks;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of symbol
     */ 
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set the value of symbol
     *
     * @return  self
     */ 
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function setTasks(ArrayCollection $tasks): self
    {
        $this->tasks = $tasks;
        return $this;
    }

    public function getTasks(): ?ArrayCollection
    {
        return $this->tasks;
    }

    /**
     * Get the value of company
     */ 
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * Set the value of companyId
     *
     * @return  self
     */ 
    public function setCompany(Company $company): self
    {
        $this->company = $company;
        return $this;
    }
}