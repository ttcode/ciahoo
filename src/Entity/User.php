<?php 

namespace App\Entity;

use App\Multitenancy\TenantAware;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User extends BaseUser implements TenantAware
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=150, nullable=true, options={"default"=""})
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=150, nullable=true, options={"default"=""})
     */
    protected $lastname;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    public function __construct()
    {
        parent::__construct();
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setFirstname($firstname = '')
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function setLastname($lastname = '')
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get the value of company
     */ 
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * Set the value of company
     *
     * @return  self
     */ 
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }
}