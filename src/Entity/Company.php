<?php

namespace App\Entity;

use App\Multitenancy\Tenant;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="company")
 * @UniqueEntity("nip")
 * @UniqueEntity("email")
 * @UniqueEntity("regon")
 * @Serializer\ExclusionPolicy("all")
 */
class Company implements Tenant
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @Serializer\Expose
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     * @Serializer\Expose
     */
    protected $nip;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     * @Serializer\Expose
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, unique=true)
     * @Serializer\Expose
     */
    protected $regon;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @Serializer\Expose
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Serializer\Expose
     */
    protected $street;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Serializer\Expose
     */
    protected $houseNumber;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    protected $isActive;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     * @Serializer\Expose
     */
    protected $createDate;

    /**
     * Get the value of id
     */ 
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of nip
     */ 
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set the value of nip
     *
     * @return  self
     */ 
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get the value of regon
     */ 
    public function getRegon()
    {
        return $this->regon;
    }

    /**
     * Set the value of regon
     *
     * @return  self
     */ 
    public function setRegon($regon)
    {
        $this->regon = $regon;

        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of street
     */ 
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set the value of street
     *
     * @return  self
     */ 
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get the value of houseNumber
     */ 
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * Set the value of houseNumber
     *
     * @return  self
     */ 
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * Get the value of isActive
     */ 
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set the value of isActive
     *
     * @return  self
     */ 
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get the value of createDate
     */ 
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set the value of createDate
     *
     * @return  self
     */ 
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}