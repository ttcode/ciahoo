<?php

namespace App\Service;

use App\Entity\Company;
use App\Repository\CompanyRepository;
use App\Service\PaginatorService;

class CompanyService 
{

    private $companyRepository;
    private $paginatorService;
    private $emailService;

    public function __construct(CompanyRepository $companyRepository, PaginatorService $paginatorService, EmailService $emailService)
    {
        $this->companyRepository = $companyRepository;
        $this->paginatorService = $paginatorService;
        $this->emailService = $emailService;
    }

    public function getCompanies(?array $params): array
    {
        $this->paginatorService->setParams($params);
        return $this->companyRepository->findAllPaged(
            $this->paginatorService->getPage(), 
            $this->paginatorService->getSize(),
            $this->paginatorService->getSearch()
        );
    }

    public function getCompaniesAll(): array
    {
        return $this->companyRepository->findAll();
    }

    public function getCompany(int $id): ?Company
    {
        return $this->companyRepository->findById($id);
    }

    public function addCompany(array $data): Company
    {
        $company = new Company();
        $company
            ->setName($data['name'])
            ->setNip($data['nip'])
            ->setIsActive(1)
            ->setCity($data['city'])
            ->setHouseNumber($data['house_number'])
            ->setStreet($data['street'])
            ->setEmail($data['email'])
            ->setCreateDate(new \DateTime());

        $this->companyRepository->save($company);
        $this->emailService->sendMessage([$data['email']]
            , "Gratulacje! Zarejestrowano firmę " . $data['name'] . '!'
            , 'Rejestracja w Ciahoo');
        return $company;
    }

    public function updateCompany(int $id, array $data): ?Company
    {
        $company = $this->companyRepository->findById($id);

        if (!$company) {
            return null;
        }

        $company
            ->setName($data['name'])
            ->setNip($data['nip'])
            ->setIsActive($data['is_active'])
            ->setCity($data['city'])
            ->setEmail($data['email'])
            ->setHouseNumber($data['house_number'])
            ->setStreet($data['street']);

        $this->companyRepository->save($company);
        return $company;
    }

    public function deleteCompany(int $id): ?Company
    {
        $company = $this->companyRepository->findById($id);

        if (!$company){
            return null;
        }

        $this->companyRepository->delete($company);
        return $company;
    }

}