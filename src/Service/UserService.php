<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\PaginatorService;
use App\Service\ConfigurationService;
use Exception;

class UserService 
{

    private $userRepository;
    private $paginatorService;
    private $configurationService;

    public function __construct(UserRepository $userRepository, PaginatorService $paginatorService, ConfigurationService $configurationService)
    {
        $this->userRepository = $userRepository;
        $this->paginatorService = $paginatorService;
        $this->configurationService = $configurationService;
    }

    public function getUsers(?array $params): array
    {
        $this->paginatorService->setParams($params);
        return $this->userRepository->findAllPaged(
            $this->paginatorService->getPage(), 
            $this->paginatorService->getSize(),
            $this->paginatorService->getSearch()
        );
    }

    public function getUsersAll(): array
    {
        return $this->userRepository->findAll();
    }

    public function getUser(int $id): ?User
    {
        return $this->userRepository->findById($id);
    }

    public function addUser(array $data): User
    {

        if (!$this->checkCompanyLimit())
        {
            throw new Exception("User limit reached");
        }

        $user = new User();
        $user->setUsername($data['username'])
            ->setEmail($data['email'])
            ->setFirstname($data['firstname'])
            ->setLastname($data['lastname'])
            ->setPlainPassword($data['password'])
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setSuperAdmin(false);

        $this->userRepository->save($user);
        return $user;
    }

    public function updateUser(int $id, array $data): ?User
    {
        $user = $this->userRepository->findById($id);

        if (!$user) {
            return null;
        }

        $user
            ->setUsername($data['username'])
            ->setEmail($data['email'])
            ->setFirstname($data['firstname'])
            ->setLastname($data['lastname']);
        $this->userRepository->save($user);
        return $user;
    }

    public function deleteUser(int $id): ?User
    {
        $user = $this->userRepository->findById($id);

        if (!$user){
            return null;
        }

        $this->userRepository->delete($user);
        return $user;
    }

    private function checkCompanyLimit(): bool
    {
        return true;
    }

}