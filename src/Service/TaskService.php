<?php

namespace App\Service;

use App\Entity\Client;
use App\Entity\Task;
use App\Entity\TaskType;
use App\Entity\User;
use App\Repository\TaskRepository;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use App\Repository\TaskTypeRepository;
use Exception;

class TaskService 
{

    private $taskRepository;
    private $productService;
    private $clientRepository;
    private $userRepository;
    private $taskTypeRepository;
    private $paginatorService;
    private $configurationService;

    public function __construct(
        TaskRepository $taskRepository, 
        ProductService $productService,
        PaginatorService $paginatorService,
        ClientRepository $clientRepository,
        TaskTypeRepository $taskTypeRepository,
        UserRepository $userRepository,
        ConfigurationService $configurationService
    )
    {
        $this->taskRepository = $taskRepository;
        $this->productService = $productService;
        $this->paginatorService = $paginatorService;
        $this->clientRepository = $clientRepository;
        $this->taskTypeRepository = $taskTypeRepository;
        $this->userRepository = $userRepository;
        $this->configurationService = $configurationService;
    }

    public function getTasks(?array $params): array
    {
        $this->paginatorService->setParams($params);
        return $this->taskRepository->findAllPaged(
            $this->paginatorService->getPage(), 
            $this->paginatorService->getSize(),
            $this->paginatorService->getSearch()
        );
    }

    public function getTasksAll(): array
    {
        return $this->taskRepository->findAll();
    }

    public function getTask(int $id): ?Task
    {
        return $this->taskRepository->findById($id);
    }

    public function addTask(array $data): Task
    {
        $formattedDate = \DateTime::createFromFormat(Task::DATE_FORMAT, $data['planned_date']);

        if ($this->checkPlannedDateIsAvailable($formattedDate) === false) {
            throw new Exception('Planned date is not available');
        }

        $task = new Task();
        $task->setDescription($data['description'])->setPlannedDate($formattedDate);
        
        if ($data['products']) {
            foreach ($data['products'] as $product) {
                $task->addProduct($this->productService->getProduct($product['id']));
            }
        }

        if ($data['client_id']) {
            $client = $this->clientRepository->findOneById($data['client_id']);
            $task->setClient($client);
        }

        if ($data['user_id']) {
            $user = $this->userRepository->findById($data['user_id']);
            $task->setUser($user);
        }

        if ($data['task_type_id']) {
            $taskType = $this->taskTypeRepository->findOneById($data['task_type_id']);
            $task->setTaskType($taskType);
        }

        $this->taskRepository->save($task);
        return $task;
    }

    public function updateTask(int $id, array $data): ?Task
    {
        $task = $this->taskRepository->findById($id);

        if (!$task) {
            return null;
        }

        $formattedDate = \DateTime::createFromFormat(Task::DATE_FORMAT, $data['planned_date']);
        $task->setDescription($data['description'])->setPlannedDate($formattedDate);
        $task->removeProducts();

        if ($data['products']) {
            foreach ($data['products'] as $product) {
                $task->addProduct($this->productService->getProduct($product['id']));
            }
        }

        if ($data['client_id']) {
            $client = new Client();
            $client->setId($data['client_id']);
            $task->setClient($client);
        }

        if ($data['user_id']) {
            $user = new User();
            $user->setId($data['user_id']);
            $task->setUser($user);
        }

        if ($data['task_type_id']) {
            $taskType = new TaskType();
            $taskType->setId($data['task_type_id']);
            $task->setTaskType($taskType);
        }

        $this->taskRepository->save($task);
        return $task;
    }

    public function deleteTask(int $id): ?Task
    {
        $task = $this->taskRepository->findById($id);

        if (!$task){
            return null;
        }

        $this->taskRepository->delete($task);
        return $task;
    }

    public function startTask(Task $task)
    {
        $startDate = $this->createCurrentTimeToSave();
        $task->setStart($startDate);
        return $this->taskRepository->save($task);
    }

    public function completeTask(Task $task)
    {
        $startDate = $this->createCurrentTimeToSave();
        $task->setComplete($startDate);
        return $this->taskRepository->save($task);
    }

    private function checkPlannedDateIsAvailable($plannedDate): bool
    {
        $countOfTasksOnPlannedDate = $this->taskRepository->getTasksCountOnPlannedDate($plannedDate);
        if ($countOfTasksOnPlannedDate['quantity'] === 0) {
            return true;
        }
        return false;
    }

    private function createCurrentTimeToSave(): \DateTime
    {
        $format = $this->configurationService->get('APP_DATE_FORMAT');
        return \DateTime::createFromFormat($format, date($format));
    }

}