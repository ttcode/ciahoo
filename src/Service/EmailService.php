<?php

namespace App\Service;

use Swift_Mailer;
use Swift_Message;
use App\Service\ConfigurationService;

class EmailService
{

    private $configurationService;
    private $mailer;

    public function __construct(ConfigurationService $configurationService, Swift_Mailer $mailer)
    {
        $this->configurationService = $configurationService;
        $this->mailer = $mailer;
    }

    public function sendMessage(array $to = [], string $txt = '', string $subject = ''): int
    {

        if (empty($to)) {
            return 0;
        }

        $message = new Swift_Message();
        $message
            ->setFrom($this->configurationService->get('APP_EMAIL_FROM'))
            ->setTo($to)
            ->setBody($txt)
            ->setSubject($subject);

        $resp = $this->mailer->send($message);
        return $resp;
    }

}