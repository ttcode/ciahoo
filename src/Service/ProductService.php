<?php

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class ProductService 
{

    private $productRepository;
    private $paginatorService;

    public function __construct(ProductRepositoryInterface $productRepository, PaginatorService $paginatorService)
    {
        $this->productRepository = $productRepository;
        $this->paginatorService = $paginatorService;
    }

    public function getProduct(int $id): ?Product
    {
        return $this->productRepository->findById($id);
    }

    public function getProducts(?array $params): array
    {
        $this->paginatorService->setParams($params);
        return $this->productRepository->findAllPaged(
            $this->paginatorService->getPage(), 
            $this->paginatorService->getSize(),
            $this->paginatorService->getSearch()
        );
    }

    public function getAllProducts(): array {
        return $this->productRepository->findAll();
    }

    public function addProduct(string $name, string $description, string $symbol): Product {
        $product = new Product();
        $product->setName($name);
        $product->setDescription($description);
        $product->setSymbol($symbol);
        $this->productRepository->save($product);

        return $product;
    }

    public function updateProduct(int $productId, string $name, string $description, string $symbol): ?Product {
        $product = $this->productRepository->findById($productId);
        if (!$product) {
            return null;
        }

        $product->setName($name);
        $product->setDescription($description);
        $product->setSymbol($symbol);
        $this->productRepository->save($product);

        return $product;
    }

    public function deleteProduct(int $id): ?Product {
        $product = $this->productRepository->findById($id);
        
        if (!$product) {
            return null;
        }

        $this->productRepository->delete($product);
        return $product;
    }

}