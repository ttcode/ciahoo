<?php

namespace App\Service;

use DateTime;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;

class TimetableService 
{

    private $taskRepository;
    private $userRepository;

    public function __construct(TaskRepository $taskRepository, UserRepository $userRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->userRepository = $userRepository;
    }

    public function checkUserWithFreeTime(DateTime $datetime = null): ?array
    {
        if ($datetime === null) {
            return null;
        }

        return [];
    }

}