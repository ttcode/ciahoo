<?php

namespace App\Service;

class PaginatorService {

    const DEFAULT_SIZE = 10;
    const DEFAULT_PAGE = 0;

    private $params;
    private $size;
    private $page;
    private $search;

    public function setParams(array $params): void
    {
        $this->params = $params;
        $this->size = $params['size'] ?? self::DEFAULT_SIZE;
        $this->page = $this->size * (isset($params['page']) ? ($params['page'] - 1) : self::DEFAULT_PAGE);
        $this->search = $params['search'] ?? '';
    }

    public function getPage(): int {
        return $this->page;
    }

    public function getSize(): int {
        return $this->size;
    }

    public function getSearch(): string {
        return $this->search;
    }

}