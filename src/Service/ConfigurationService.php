<?php

namespace App\Service;

use Symfony\Component\Yaml\Yaml;

class ConfigurationService 
{
    CONST CONFIG_DIRECTORY = __DIR__ . '/../../config/appconfig.yaml';

    private $config;

    public function __construct()
    {
        $this->prepareConfigArray();
    }

    public function get($key)
    {
        if (!isset($this->config[$key]))
        {
            return false;
        }

        return $this->config[$key];
    }

    protected function prepareConfigArray(): void
    {
        $this->config = Yaml::parse(file_get_contents(self::CONFIG_DIRECTORY));
    }

}