<?php

namespace App\Service;

use App\Entity\Client;
use App\Repository\ClientRepositoryInterface;

class ClientService {
    
    private $clientRepository;
    private $paginatorService;

    public function __construct(ClientRepositoryInterface $clientRepository, PaginatorService $paginatorService)
    {
        $this->clientRepository = $clientRepository;
        $this->paginatorService = $paginatorService;
    }

    public function getAllClients(): array
    {
        return $this->clientRepository->findAll();
    }

    public function getClients(?array $params): array
    {
        $this->paginatorService->setParams($params);
        return $this->clientRepository->findAllPaged(
            $this->paginatorService->getPage(), 
            $this->paginatorService->getSize(),
            $this->paginatorService->getSearch()
        );
    }

    public function getClient(int $id): ?Client
    {
        return $this->clientRepository->findOneById($id);
    }

    public function addClient(string $firstName, string $lastName, ?int $state): ?Client
    {
        $client = new Client();
        $client->setFirstName($firstName);
        $client->setLastName($lastName);
        $client->setState($state ?? 1);

        $this->clientRepository->save($client);
        return $client;
    }

    public function updateClient(int $id, string $firstname, string $lastname, ?int $state): ?Client{
        $client = $this->clientRepository->findOneById($id);

        if (!$client) {
            return null;
        }

        $client->setFirstName($firstname);
        $client->setLastName($lastname);
        
        if (isset($state)) {
            $client->setState($state);
        }

        $this->clientRepository->save($client);
        return $client;
    }

    public function deleteClient(int $id): ?Client
    {
        $client = $this->clientRepository->findOneById($id);

        if (!$client) {
            return null;
        }

        $this->clientRepository->delete($client);
        return $client;
    }

}