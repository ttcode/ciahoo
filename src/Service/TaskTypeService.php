<?php

namespace App\Service;

use App\Entity\TaskType;
use App\Repository\TaskTypeRepositoryInterface;

class TaskTypeService {
    
    private $dictionaryRepository;
    private $paginatorService;

    public function __construct(TaskTypeRepositoryInterface $dictionaryRepository, PaginatorService $paginatorService)
    {
        $this->dictionaryRepository = $dictionaryRepository;
        $this->paginatorService = $paginatorService;
    }

    public function getAllTaskTypes(): array
    {
        return $this->dictionaryRepository->findAll();
    }

    public function getTaskTypes(?array $params): array
    {
        $this->paginatorService->setParams($params);
        return $this->dictionaryRepository->findAllPaged(
            $this->paginatorService->getPage(), 
            $this->paginatorService->getSize(),
            $this->paginatorService->getSearch()
        );
    }

    public function getTaskType(int $id): ?TaskType
    {
        return $this->dictionaryRepository->findOneById($id);
    }

    public function addTaskType(array $data): ?TaskType
    {
        $taskType = new TaskType();
        $taskType
            ->setType($data['type'])
            ->setName($data['name']);

        if (isset($data['estimated_time'])) {
            $taskType->setEstimatedTime($data['estimated_time']);
        }

        $this->dictionaryRepository->save($taskType);
        return $taskType;
    }

    public function updateTaskType(int $id, array $data): ?TaskType
    {
        $taskType = $this->dictionaryRepository->findOneById($id);

        if (!$taskType) {
            return null;
        }

        $taskType->setType($data['type']);
        $taskType->setName($data['name']);

        if (isset($data['estimated_time'])) {
            $taskType->setEstimatedTime($data['estimated_time']);
        }
        
        $this->dictionaryRepository->save($taskType);
        return $taskType;
    }

    public function deleteTaskType(int $id): ?TaskType
    {
        $taskType = $this->dictionaryRepository->findOneById($id);

        if (!$taskType) {
            return null;
        }

        $this->dictionaryRepository->delete($taskType);
        return $taskType;
    }

}