<?php

namespace App\EventListener;

use App\Multitenancy\TenantContext;
use App\Multitenancy\TenantResolver;
use App\Multitenancy\TokenDecoder;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpFoundation\Response;

class RequestListener
{

    private $resolver;
    private $companyContext;
    private $entityManager;
    private $decoder;
    private $userRepository;

    public function __construct(TenantResolver $resolver, TenantContext $tenantContext, 
        EntityManagerInterface $entityManager, TokenDecoder $decoder, UserRepository $userRepository)
    {
        $this->resolver = $resolver;
        $this->companyContext = $tenantContext;
        $this->entityManager = $entityManager;
        $this->decoder = $decoder;
        $this->userRepository = $userRepository;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $authHeader = $request->headers->get('authorization');

        if ($authHeader === null || $authHeader === '') {
            $event->setResponse(new Response('', Response::HTTP_UNAUTHORIZED));
            return;
        }

        $decodedUsername = $this->decoder->decode($authHeader);

        try {
            $user = $this->userRepository->findByUsername($decodedUsername);
            $tenant = $this->resolver->resolve($user);
            $this->companyContext->initialize($tenant);
            $filter = $this->entityManager->getFilters()->getFilter('tenant_filter');
            $filter->setCompany($tenant);
        } catch (Exception $e) {
            $event->setResponse(new Response('Company not found', Response::HTTP_NOT_FOUND));
        }
    }
}