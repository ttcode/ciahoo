<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use App\Service\CompanyService;
use App\Service\UserService;
use App\Service\EmailService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AuthController extends AbstractFOSRestController
{

    private $userService;
    private $companyService;

    public function __construct(UserService $userService, CompanyService $companyService, EmailService $emailService)
    {
        $this->userService = $userService;
        $this->companyService = $companyService;
    }

    /**
     * Register company.
     * @FOSRest\Post("/auth/register")
     */
    public function register(Request $request)
    {
        $data = $request->request->all();
        
        if (!isset($data['company'])) {
            throw new BadRequestHttpException('Company data not specified');
        }

        if (!isset($data['user'])) {
            throw new BadRequestHttpException('User data not specified');
        }

        $company = $this->companyService->addCompany($data['company']);
        $companyId = $company->getId();

        $data['user']['company_id'] = $companyId;
        $user = $this->userService->addUser($data['user']);

        return $this->handleView($this->view([$company, $user]));
    }
}