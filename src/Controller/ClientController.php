<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Service\ClientService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ClientController extends AbstractFOSRestController {

    private $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;    
    }

    /**
     * Lists all Clients.
     * @FOSRest\Get("/clientsall")
     */
    public function getClientsAllAction(): Response
    {
        $clients = $this->clientService->getAllClients(); 
        return $this->handleView($this->view($clients));
    }

    /**
     * Get tasks list with pagination and search.
     * @FOSRest\Get("/clients")
     */
    public function getClientsAction(Request $request): Response
    {
        $params = $request->query->all();
        $tasks = $this->clientService->getClients($params);
        return $this->handleView($this->view($tasks));
    }

    /**
     * Get client.
     * @FOSRest\Get("/clients/{id}")
     */
    public function getClientAction(int $id): Response
    {
        $client = $this->clientService->getClient($id);

        if (!$client) {
            throw new EntityNotFoundException('Client not found.');
        }

        return $this->handleView($this->view($client));
    }

    /**
     * Create client.
     * @FOSRest\Post("/clients")
     */
    public function createClientAction(Request $request): Response
    {
        if (null === $request->get('firstname')) {
            throw new BadRequestHttpException("Client firstName not specified.");
        }

        if (null === $request->get('lastname')) {
            throw new BadRequestHttpException("Client lastName not specified.");
        }

        $client = $this->clientService->addClient(
            $request->get('firstname'),
            $request->get('lastname'),
            $request->get('state')
        );

        return $this->handleView($this->view($client, Response::HTTP_CREATED));
    }

    /**
     * Edit client.
     * @FOSRest\Put("/clients/{id}")
     */
    public function editClientAction(int $id, Request $request): Response
    {
        if (null === $request->get('firstname')) {
            throw new BadRequestHttpException("Firstname not specified.");
        }

        if (null === $request->get('lastname')) {
            throw new BadRequestHttpException("Lastname not specified.");
        }

        $client = $this->clientService->updateClient(
            $id, 
            $request->get('firstname'), 
            $request->get('lastname'), 
            $request->get('state')
        );

        return $this->handleView($this->view($client));  
    }

    /**
     * Delete client.
     * @FOSRest\Delete("/clients/{id}")
     */
    public function deleteClientAction(int $id)
    {
        $client = $this->clientService->deleteClient($id);

        if (!$client) {
            throw new EntityNotFoundException('Client not found.');
        }

        return $this->handleView($this->view($client, Response::HTTP_OK));
    }
}