<?php

namespace App\Controller;

use App\Service\TaskService;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TaskController extends AbstractFOSRestController {

    private $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * Get tasks list with pagination and search.
     * @FOSRest\Get("/tasks")
     */
    public function getTasksAction(Request $request): Response
    {
        $params = $request->query->all();
        $tasks = $this->taskService->getTasks($params);
        return $this->handleView($this->view($tasks));
    }

    /**
     * Get tasks all list.
     * @FOSRest\Get("/tasksall")
     */
    public function getTasksAllAction(): Response
    {
        $tasks = $this->taskService->getTasksAll();
        return $this->handleView($this->view($tasks)); 
    }

    /**
     * Get task.
     * @FOSRest\Get("/tasks/{id}")
     */
    public function getTaskAction(int $id): Response
    {
        $task = $this->taskService->getTask($id);

        if (!$task) {
            throw new EntityNotFoundException("Task not found.");
        }

        return $this->handleView($this->view($task));
    }

    /**
     * Add new task.
     * @FOSRest\Post("/tasks")
     */
    public function createTaskAction(Request $request): Response 
    {
        if (null === $request->get('description')) {
            throw new BadRequestHttpException('description not specified');
        }

        if (null === $request->get('planned_date')) {
            throw new BadRequestHttpException('planned_date not specified');
        }

        $task = $this->taskService->addTask($request->request->all());

        return $this->handleView($this->view($task), Response::HTTP_CREATED);
    }

    /**
     * Edit task.
     * @FOSRest\Put("/tasks/{id}")
     */
    public function editTaskAction(int $id, Request $request)
    {
        if (null === $request->get('description')) {
            throw new BadRequestHttpException('description not specified');
        }

        if (null === $request->get('planned_date')) {
            throw new BadRequestHttpException('planned_date not specified');
        }

        $task = $this->taskService->updateTask(
            $id,
            $request->request->all()
        );

        return $this->handleView($this->view($task));
    }

    /**
     * Delete task.
     * @FOSRest\Delete("/tasks/{id}")
     */
    public function deleteTaskAction(int $id)
    {
        $task = $this->taskService->deleteTask($id);

        if (!$task){
            throw new EntityNotFoundException('Task not found');
        }

        return $this->handleView($this->view($task));
    }

    /**
     * Start task.
     * @FOSRest\Put("/tasks/start/{id}")
     */
    public function startTask(int $id)
    {
        $task = $this->taskService->getTask($id);

        if (!$task) {
            throw new EntityNotFoundException('Task not found');
        }

        $this->taskService->startTask($task);
        return $this->handleView($this->view($task));
    }

    /**
     * Start task.
     * @FOSRest\Put("/tasks/complete/{id}")
     */
    public function completeTask(int $id)
    {
        $task = $this->taskService->getTask($id);

        if (!$task) {
            throw new EntityNotFoundException('Task not found');
        }

        $this->taskService->completeTask($task);
        return $this->handleView($this->view($task));
    }

}