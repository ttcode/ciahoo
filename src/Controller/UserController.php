<?php

namespace App\Controller;

use App\Service\UserService;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserController extends AbstractFOSRestController {

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get users list with pagination and search.
     * @FOSRest\Get("/users")
     */
    public function getUsersAction(Request $request): Response
    {
        $params = $request->query->all();
        $users = $this->userService->getUsers($params);
        return $this->handleView($this->view($users));
    }

    /**
     * Get users all list.
     * @FOSRest\Get("/usersall")
     */
    public function getUsersAllAction(): Response
    {
        $users = $this->userService->getUsersAll();
        return $this->handleView($this->view($users)); 
    }

    /**
     * Get user.
     * @FOSRest\Get("/users/{id}")
     */
    public function getUserAction(int $id): Response
    {
        $user = $this->userService->getUser($id);

        if (!$user) {
            throw new EntityNotFoundException("User not found.");
        }

        return $this->handleView($this->view($user));
    }

    /**
     * Add new user.
     * @FOSRest\Post("/users")
     */
    public function createUserAction(Request $request): Response 
    {
        if (null === $request->get('username')) {
            throw new BadRequestHttpException('username not specified');
        }

        if (null === $request->get('email')) {
            throw new BadRequestHttpException('email not specified');
        }

        $user = $this->userService->addUser(
            $request->request->all()
        );

        return $this->handleView($this->view($user), Response::HTTP_CREATED);
    }

    /**
     * Edit user.
     * @FOSRest\Put("/users/{id}")
     */
    public function editUserAction(int $id, Request $request)
    {
        if (null === $request->get('username')) {
            throw new BadRequestHttpException('username not specified');
        }

        if (null === $request->get('email')) {
            throw new BadRequestHttpException('email not specified');
        }

        $user = $this->userService->updateUser(
            $id,
            $request->get('username'),
            $request->get('email'),
            $request->get('firstname'),
            $request->get('lastname')
        );

        return $this->handleView($this->view($user));
    }

    /**
     * Delete user.
     * @FOSRest\Delete("/users/{id}")
     */
    public function deleteUserAction(int $id)
    {
        $user = $this->userService->deleteUser($id);

        if (!$user){
            throw new EntityNotFoundException('User not found');
        }

        return $this->handleView($this->view($user));
    }

}