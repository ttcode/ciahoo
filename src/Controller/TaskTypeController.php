<?php

namespace App\Controller;

use App\Service\TaskTypeService;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TaskTypeController extends AbstractFOSRestController {

    private $taskTypeService;

    public function __construct(TaskTypeService $taskTypeService)
    {
        $this->taskTypeService = $taskTypeService;
    }

    /**
     * Get TaskType list with pagination and search.
     * @FOSRest\Get("/tasktypes")
     */
    public function getTaskTypesAction(Request $request): Response
    {
        $params = $request->query->all();
        $taskTypes = $this->taskTypeService->getTaskTypes($params);
        return $this->handleView($this->view($taskTypes));
    }

    /**
     * Get TaskType all list.
     * @FOSRest\Get("/tasktypesall")
     */
    public function getTaskTypesAllAction(): Response
    {
        $taskTypes = $this->taskTypeService->getAllTaskTypes();
        return $this->handleView($this->view($taskTypes)); 
    }

    /**
     * Get TaskType.
     * @FOSRest\Get("/tasktypes/{id}")
     */
    public function getTaskType(int $id): Response
    {
        $taskType = $this->taskTypeService->getTaskType($id);

        if (!$taskType) {
            throw new EntityNotFoundException("TaskType not found.");
        }

        return $this->handleView($this->view($taskType));
    }

    /**
     * Add new TaskType.
     * @FOSRest\Post("/tasktypes")
     */
    public function createTaskTypeAction(Request $request): Response 
    {
        if (null === $request->get('name')) {
            throw new BadRequestHttpException('name not specified');
        }

        if (null === $request->get('type')) {
            throw new BadRequestHttpException('type not specified');
        }

        $taskType = $this->taskTypeService->addTaskType(
            $request->request->all()
        );

        return $this->handleView($this->view($taskType), Response::HTTP_CREATED);
    }

    /**
     * Edit TaskType.
     * @FOSRest\Put("/tasktypes/{id}")
     */
    public function editTaskTypeAction(int $id, Request $request)
    {
        if (null === $request->get('name')) {
            throw new BadRequestHttpException('name not specified');
        }

        if (null === $request->get('type')) {
            throw new BadRequestHttpException('type not specified');
        }

        $taskType = $this->taskTypeService->updateTaskType(
            $id,
            $request->request->all()
        );

        return $this->handleView($this->view($taskType));
    }

    /**
     * Delete TaskType.
     * @FOSRest\Delete("/tasktypes/{id}")
     */
    public function deleteTaskTypeAction(int $id)
    {
        $taskType = $this->taskTypeService->deleteTaskType($id);

        if (!$taskType){
            throw new EntityNotFoundException('TaskType not found');
        }

        return $this->handleView($this->view($taskType));
    }

}