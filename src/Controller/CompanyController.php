<?php

namespace App\Controller;

use App\Service\CompanyService;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CompanyController extends AbstractFOSRestController {

    private $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * Get companies list with pagination and search.
     * @FOSRest\Get("/companies")
     */
    public function getCompaniesAction(Request $request): Response
    {
        $params = $request->query->all();
        $companies = $this->companyService->getCompanies($params);
        return $this->handleView($this->view($companies));
    }

    /**
     * Get companies all list.
     * @FOSRest\Get("/companiesall")
     */
    public function getCompaniesAllAction(): Response
    {
        $companies = $this->companyService->getCompaniesAll();
        return $this->handleView($this->view($companies)); 
    }

    /**
     * Get company.
     * @FOSRest\Get("/companies/{id}")
     */
    public function getCompanyAction(int $id): Response
    {
        $company = $this->companyService->getCompany($id);

        if (!$company) {
            throw new EntityNotFoundException("Company not found.");
        }

        return $this->handleView($this->view($company));
    }

    /**
     * Add new company.
     * @FOSRest\Post("/companies")
     */
    public function createCompanyAction(Request $request): Response 
    {
        if (null === $request->get('name')) {
            throw new BadRequestHttpException('name not specified');
        }

        if (null === $request->get('nip')) {
            throw new BadRequestHttpException('nip not specified');
        }

        $company = $this->companyService->addCompany(
            $request->request->all()
        );

        return $this->handleView($this->view($company), Response::HTTP_CREATED);
    }

    /**
     * Edit company.
     * @FOSRest\Put("/companies/{id}")
     */
    public function editCompaniesAction(int $id, Request $request)
    {
        if (null === $request->get('name')) {
            throw new BadRequestHttpException('name not specified');
        }

        if (null === $request->get('nip')) {
            throw new BadRequestHttpException('nip not specified');
        }

        $company = $this->companyService->updateCompany(
            $id,
            $request->request->all()
        );

        return $this->handleView($this->view($company));
    }

    /**
     * Delete company.
     * @FOSRest\Delete("/companies/{id}")
     */
    public function deleteCompanyAction(int $id)
    {
        $company = $this->companyService->deleteCompany($id);

        if (!$company){
            throw new EntityNotFoundException('Company not found');
        }

        return $this->handleView($this->view($company));
    }

}