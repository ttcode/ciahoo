<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ProductService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductController extends AbstractFOSRestController {

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Lists all Products.
     * @FOSRest\Get("/productsall")
     */
    public function getProductsAllAction(): Response
    {
        $products = $this->productService->getAllProducts();
        return $this->handleView($this->view($products));
    }

    /**
     * Get tasks list with pagination and search.
     * @FOSRest\Get("/products")
     */
    public function getProductsAction(Request $request): Response
    {
        $params = $request->query->all();
        $tasks = $this->productService->getProducts($params);
        return $this->handleView($this->view($tasks));
    }

    /**
     * Get product.
     * @FOSRest\Get("/products/{id}")
     */
    public function getProductAction(int $id): Response
    {

        $product = $this->productService->getProduct($id);

        if (!$product) {
            throw new EntityNotFoundException("Product not found.");
        }

        return $this->handleView($this->view($product));
    }

    /**
     * Add new product.
     * @FOSRest\Post("/products")
     */
    public function createProductAction(Request $request): Response
    {

        if (null === $request->get('name')) {
            throw new BadRequestHttpException("Product name not specified.");
        }

        if (null === $request->get('description')) {
            throw new BadRequestHttpException("Product description not specified.");
        }

        if (null === $request->get('symbol')) {
            throw new BadRequestHttpException("Product symbol not specified.");
        }

        $product = $this->productService->addProduct(
            $request->get('name'), 
            $request->get('description'), 
            $request->get('symbol')
        );

        return $this->handleView($this->view($product));
    }

    /**
     * Delete product.
     * @FOSRest\Delete("/products/{id}")
     */
    public function deleteProductAction(int $id): Response
    {
        $product = $this->productService->deleteProduct($id);

        if (!$product) {
            throw new EntityNotFoundException('Product not found.');
        }

        return $this->handleView($this->view($product));        
    }

    /**
     * Edit product.
     * @FOSRest\Put("/products/{id}")
     */
    public function editProductAction(int $id, Request $request): Response
    {

        if (null === $request->get('name')) {
            throw new BadRequestHttpException("Product name not specified.");
        }

        if (null === $request->get('description')) {
            throw new BadRequestHttpException("Product description not specified.");
        }

        if (null === $request->get('symbol')) {
            throw new BadRequestHttpException("Product symbol not specified.");
        }

        $product = $this->productService->updateProduct(
            $id, 
            $request->get('name'), 
            $request->get('description'), 
            $request->get('symbol')
        );

        return $this->handleView($this->view($product));        
    }
}